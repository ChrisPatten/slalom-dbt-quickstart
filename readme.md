# Slalom dbt Quickstart

# Getting Started
To begin, clone this repository locally. Then, you'll need to set up your Python
environment. On a Mac, use [pyenv](https://akrabat.com/creating-virtual-environments-with-pyenv/#install-pyenv).

## Pyenv
First, install Python 3.10. `pyenv install 3.10.6`. In your terminal, navigate to
this directory and run `pyenv virtualenv 3.10.6 slalom-dbt-quickstart`. This will
create a dedicated virtual environment using Python 3.10.6. To select that environment
automatically when working in this project, run `pyenv local slalom-dbt-quickstart`.

## PIP
Once in the virtual environment you've created, run `pip install -r requirements.txt`
to install the correct versions of the packages for this dbt project.