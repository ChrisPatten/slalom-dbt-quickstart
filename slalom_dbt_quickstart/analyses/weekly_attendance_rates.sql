WITH fact_student_attendance AS (
    select * from {{ ref('fact_student_attendance') }}
)
, dim_student AS (
    select * from {{ ref('dim_student') }}
)
, dim_school AS (
    select * from {{ ref('dim_school') }}
)
, totals AS (
    select
        dim_school_key,
        date_trunc('week', calendar_date) as calendar_week,
        sum(instructional_day_value) as student_days,
        sum(eligible_periods) as total_periods,
        sum(excused_absense_periods) as total_excused_periods,
        sum(unexcused_absense_periods) as total_unexcused_periods,
        sum(tardy_periods) as total_tardy_periods
    from fact_student_attendance
    group by 1, 2
)
select
    dim_school.school_name,
    totals.calendar_week,
    totals.total_excused_periods / totals.total_periods + 0.0 AS pct_absent_excused,
    totals.total_unexcused_periods / totals.total_periods + 0.0 AS pct_absent_unexcused,
    (totals.total_excused_periods + totals.total_unexcused_periods) / totals.total_periods + 0.0 AS pct_absent_total
from totals
join dim_school
    on dim_school.dim_school_key = totals.dim_school_key