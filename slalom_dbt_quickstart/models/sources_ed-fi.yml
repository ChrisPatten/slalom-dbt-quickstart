version: 2

sources:
  - name: edfi_raw
    tables:
      - name: school
        description: >
          This entity represents an educational organization that includes staff 
          and students who participate in classes and educational activity 
          groups.
        columns:
          - name: SchoolId
            description: The identifier assigned to a school.
          
          - name: SchoolTypeDescriptorId
            description: >
              The type of education institution as classified by its primary 
              focus.
          
          - name: CharterStatusDescriptorId
            description: >
              The category of charter school. For example: School Charter, Open 
              Enrollment Charter.
          
          - name: TitleIPartASchoolDesignationDescriptorId
            description: Denotes the Title I Part A designation for the school.
          
          - name: MagnetSpecialProgramEmphasisSchoolDescriptorId
            description: >
              A school that has been designed to attract students of different 
              racial/ethnic backgrounds for the purpose of reducing, preventing 
              or eliminating racial isolation; and/or to provide an academic or 
              social focus on a particular theme (e.g., science/math, performing 
              arts, gifted/talented, or foreign language).
          
          - name: AdministrativeFundingControlDescriptorId
            description: >
              This descriptor holds the type of education institution as 
              classified by its funding source (e.g., public or private).
          
          - name: InternetAccessDescriptorId
            description: The type of Internet access available.
          
          - name: LocalEducationAgencyId
            description: The identifier assigned to a local education agency.
          
          - name: CharterApprovalAgencyTypeDescriptorId
            description: >
              The type of agency that approved the establishment or continuation 
              of a charter school.
          
          - name: CharterApprovalSchoolYear

      - name: section
        description: >
          This entity represents a setting in which organized instruction of 
          course content is provided, in-person or otherwise, to one or more 
          students for a given period of time. A course offering may be offered 
          to more than one section.
        columns:
          - name: LocalCourseCode
            description: >
              The local code assigned by the LEA that identifies the 
              organization of subject matter and related learning experiences 
              provided for the instruction of students.
          
          - name: SchoolId
            description: The identifier assigned to a school.
            tests:
              - relationships:
                  to: source('edfi_raw', 'school')
                  field: SchoolId
          
          - name: SchoolYear
          
          - name: SectionIdentifier
            description: The local identifier assigned to a section.
          
          - name: SessionName
            description: >
              The identifier for the calendar for the academic session (e.g., 
              2010/11, 2011 Summer).
          
          - name: SequenceOfCourse
            description: >
              When a Section is part of a sequence of parts for a course, the 
              number if the sequence. If the course has only one part, the value 
              of this Section attribute should be 1.
          
          - name: EducationalEnvironmentDescriptorId
            description: >
              The setting in which a child receives education and related 
              services.
          
          - name: MediumOfInstructionDescriptorId
            description: >
              The media through which teachers provide instruction to students 
              and students and teachers communicate about instructional matters.
          
          - name: PopulationServedDescriptorId
            description: >
              The type of students the Section is offered and tailored to.
          
          - name: AvailableCredits
            description: >
              Credits or units of value awarded for the completion of a course.
          
          - name: AvailableCreditTypeDescriptorId
            description: >
              The type of credits or units of value awarded for the completion 
              of a course.
          
          - name: AvailableCreditConversion
            description: >
              Conversion factor that when multiplied by the number of credits is 
              equivalent to Carnegie units.
          
          - name: InstructionLanguageDescriptorId
            description: >
              This descriptor defines the language(s) that are spoken or 
              written. It is strongly recommended that entries use only 
              ISO 639-2 language codes: for CodeValue, use the 3 character code; 
              for ShortDescription and Description use the full language name.
          
          - name: LocationSchoolId
            description: >
              This entity represents the physical space where students gather 
              for a particular class/section. The Location may be an indoor or 
              outdoor area designated for the purpose of meeting the educational 
              needs of students.
          
          - name: LocationClassroomIdentificationCode
            description: >
              A unique number or alphanumeric code assigned to a room by a 
              school, school system, state, or other agency or entity.
          
          - name: OfficialAttendancePeriod
            description: >
              Indicator of whether this class period is used for official daily 
              attendance. Alternatively, official daily attendance may be tied 
              to a Section.
          
          - name: SectionName
            description: >
              A locally-defined name for the section, generally created to make 
              the section more recognizable in informal contexts and generally 
              distinct from the SectionIdentifier.
          
          - name: Discriminator
          
          - name: CreateDate
            description: The date the record was created.
          
          - name: LastModifiedDate
            description: The date the record was last modified
          
          - name: Id
            description: The unique ID of the Section.
          
          - name: ChangeVersion

      - name: student_section_attendance_event
        description: >
          This event entity represents the recording of whether a student is in 
          attendance for a section.
        columns:
          - name: AttendanceEventCategoryDescriptorId
            description: >
              This descriptor holds the category of the attendance event (e.g., 
              tardy).
          
          - name: EventDate
            description: Date for this attendance event.
          
          - name: LocalCourseCode
            description: >
              The local code assigned by the LEA that identifies the 
              organization of subject matter and related learning experiences 
              provided for the instruction of students.
          
          - name: SchoolId
            description: The identifier assigned to a school.
            tests:
              - relationships:
                  to: source('edfi_raw', 'school')
                  field: SchoolId
          
          - name: SchoolYear
          
          - name: SectionIdentifier
            description: The local identifier assigned to a section.
            tests:
              - relationships:
                  to: source('edfi_raw', 'section')
                  field: SectionIdentifier
          
          - name: SessionName
            description: >
              The identifier for the calendar for the academic session (e.g., 
              2010/11, 2011 Summer).
          
          - name: StudentUSI
            description: >
              A unique alphanumeric code assigned to a student.
            tests:
              - relationships:
                  to: source('edfi_raw', 'student')
                  field: StudentUSI
          
          - name: AttendanceEventReason
            description: The reason for the absence or tardy.
          
          - name: EducationalEnvironmentDescriptorId
            description: >
              The setting in which a child receives education and related 
              services.
          
          - name: EventDuration
            description: >
              The amount of time for the event as recognized by the school: 1 
              day = 1, 1/2 day = 0.5, 1/3 day = 0.33.
          
          - name: SectionAttendanceDuration
            description: The duration in minutes of the section attendance event.
          
          - name: ArrivalTime
            description: >
              The time of day the student arrived for the attendance event in 
              ISO 8601 format.
          
          - name: DepartureTime
            description: >
              The time of day the student departed for the attendance event in 
              ISO 8601 format.
          
          - name: Discriminator
          
          - name: CreateDate
            description: The date the record was created.
          
          - name: LastModifiedDate
            description: The date the record was last modified
          
          - name: Id
            description: The unique ID of the Student Section Attendance Event.
          
          - name: ChangeVersion

      - name: student
        description: >
          This entity represents an individual for whom instruction, services, 
          and/or care are provided in an early childhood, elementary, or 
          secondary educational program under the jurisdiction of a school, 
          education agency or other institution or program. A student is a 
          person who has been enrolled in a school or other educational 
          institution.
        columns:
          - name: StudentUSI
            description: A unique alphanumeric code assigned to a student.

          - name: PersonalTitlePrefix
            description: >
              A prefix used to denote the title, degree, position, or seniority 
              of the person.

          - name: FirstName
            description: >
              A name given to an individual at birth, baptism, or during another 
              naming ceremony, or through legal change.

          - name: MiddleName
            description: >
              A secondary name given to an individual at birth, baptism, or 
              during another naming ceremony.

          - name: LastSurname
            description: The name borne in common by members of a family.

          - name: GenerationCodeSuffix
            description: >
              An appendage, if any, used to denote an individual's generation
              in his family (e.g., Jr., Sr., III).

          - name: MaidenName
            description: The person's maiden name.The person's maiden name.

          - name: BirthDate
            description: >
              The month, day, and year on which an individual was born.

          - name: BirthCity
            description: The city the student was born in.

          - name: BirthStateAbbreviationDescriptorId
            description: >
              The abbreviation for the name of the state (within the United 
              States) or extra-state jurisdiction in which an individual was 
              born.

          - name: BirthInternationalProvince
            description: >
              For students born outside of the U.S., the Province or 
              jurisdiction in which an individual is born.

          - name: BirthCountryDescriptorId
            description: >
              The country in which an individual is born. It is strongly 
              recommended that entries use only ISO 3166 2-letter country codes.

          - name: DateEnteredUS
            description: >
              For students born outside of the U.S., the date the student 
              entered the U.S.

          - name: MultipleBirthStatus
            description: >
              Indicator of whether the student was born with other siblings 
              (i.e., twins, triplets, etc.)

          - name: BirthSexDescriptorId
            description: A person's gender at birth.

          - name: CitizenshipStatusDescriptorId
            description: >
              An indicator of whether or not the person is a U.S. citizen.

          - name: PersonId

          - name: SourceSystemDescriptorId
            description: >
              This descriptor defines the originating record source system.

          - name: StudentUniqueId
            description: >
              A unique alphanumeric code assigned to a person by a system 
              managing unique identifiers.

          - name: Discriminator
          
          - name: CreateDate
            description: The date the record was created.
          
          - name: LastModifiedDate
            description: The date the record was last modified
          
          - name: Id
            description: The unique ID of the Student.

          - name: ChangeVersion

      - name: education_organization
        description: >
          This entity represents any public or private institution, 
          organization, or agency that provides instructional or support 
          services to students or staff at any level.

        columns:
          - name: EducationOrganizationId
            description: The identifier assigned to an education organization.

          - name: NameOfInstitution
            description: The full, legally accepted name of the institution.

          - name: ShortNameOfInstitution
            description: A short name for the institution.

          - name: WebSite
            description: >
              The public web site address (URL) for the EducationOrganization.

          - name: OperationalStatusDescriptorId
            description: >
              The current operational status of the EducationOrganization (e.g., 
              active, inactive).

          - name: Discriminator
          
          - name: CreateDate
            description: The date the record was created.
          
          - name: LastModifiedDate
            description: The date the record was last modified
          
          - name: Id
            description: The unique ID of the EducationOrganization.

          - name: ChangeVersion

      - name: student_section_association
        description: >
          This association indicates the course sections to which a student is 
          assigned.

        columns:
          - name: BeginDate
            description: >
              Month, day, and year of the Student's entry or assignment to the 
              Section.

          - name: LocalCourseCode
            description:  >
              The local code assigned by the LEA that identifies the 
              organization of subject matter and related learning experiences 
              provided for the instruction of students.

          - name: SchoolId
            description: The identifier assigned to a school.
            tests:
              - relationships:
                  to: source('edfi_raw', 'school')
                  field: SchoolId

          - name: SchoolYear

          - name: SectionIdentifier
            description: The local identifier assigned to a section.
            tests:
              - relationships:
                  to: source('edfi_raw', 'section')
                  field: SectionIdentifier

          - name: SessionName
            description: >
              The identifier for the calendar for the academic session (e.g., 
              2010/11, 2011 Summer).

          - name: StudentUSI
            description: >
              A unique alphanumeric code assigned to a student.
            tests:
              - relationships:
                  to: source('edfi_raw', 'student')
                  field: StudentUSI

          - name: EndDate
            description: >
              Month, day, and year of the withdrawal or exit of the Student from 
              the Section.

          - name: HomeroomIndicator
            description: >
              Indicates the Section is the student's homeroom. Homeroom period 
              may the convention for taking daily attendance.

          - name: RepeatIdentifierDescriptorId
            description: >
              An indication as to whether a student has previously taken a given 
              course. Repeated, counted in grade point average Repeated, not 
              counted in grade point average Not repeated Other.

          - name: TeacherStudentDataLinkExclusion
            description: >
              Indicates that the student-section combination is excluded from 
              calculation of value-added or growth attribution calculations used 
              for a particular teacher evaluation.

          - name: AttemptStatusDescriptorId
            description: >
              An indication of the student's completion status for the section.

          - name: Discriminator
          
          - name: CreateDate
            description: The date the record was created.
          
          - name: LastModifiedDate
            description: The date the record was last modified

          - name: Id
            description: The unique ID of the Student Section Association.

          - name: ChangeVersion

