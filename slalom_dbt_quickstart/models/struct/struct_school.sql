WITH seed_school AS (
  SELECT * FROM {{ source('edfi_raw', 'school') }}
)
SELECT
  SchoolId::integer AS school_id,
  LocalEducationAgencyId::integer AS district_id
FROM seed_school