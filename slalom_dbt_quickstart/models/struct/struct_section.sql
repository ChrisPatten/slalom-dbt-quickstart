WITH seed_section AS (
  SELECT * FROM {{ source('edfi_raw', 'section') }}
)
SELECT
  LocalCourseCode AS local_course_code,
  SchoolId::integer AS school_id,
  SchoolYear::integer AS school_year,
  SectionIdentifier AS section_identifier,
  SessionName AS session_name,
  SequenceOfCourse::integer AS sequence_of_course,
  EducationalEnvironmentDescriptorId::integer AS educational_environment_descriptor_id,
  AvailableCredits::numeric AS available_credits,
  LocationSchoolId AS location_school_id,
  LocationClassroomIdentificationCode AS location_classroom_identification_code,
  SectionName AS section_name,
  TO_TIMESTAMP_TZ(CreateDate || ' +0000') AS create_datetime,
  TO_TIMESTAMP_TZ(LastModifiedDate || ' +0000') AS last_modified_datetime,
  Id AS ods_section_id
FROM seed_section