WITH seed_education_organization AS (
  SELECT * FROM {{ source('edfi_raw', 'education_organization') }}
)
SELECT
  EducationOrganizationId::integer AS education_organization_id,
  NameOfInstitution AS name_of_institution,
  ShortNameOfInstitution AS short_name_of_institution,
  WebSite AS web_site,
  Discriminator AS discriminator,
  TO_TIMESTAMP_TZ(CreateDate || ' +0000') AS create_datetime,
  TO_TIMESTAMP_TZ(LastModifiedDate || ' +0000') AS last_modified_datetime,
  Id AS ods_education_organization_id
FROM seed_education_organization