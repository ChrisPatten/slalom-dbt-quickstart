WITH student_seed AS (
  SELECT * FROM {{ source('edfi_raw', 'student') }}
)
SELECT
  StudentUSI::integer AS student_usi,
  PersonalTitlePrefix AS personal_title_prefix,
  FirstName AS first_name,
  MiddleName AS middle_name,
  LastSurname AS last_surname,
  GenerationCodeSuffix AS generation_code_suffix,
  BirthDate::date AS birth_date,
  StudentUniqueId::integer AS student_id,
  TO_TIMESTAMP_TZ(CreateDate || ' +0000') AS create_datetime,
  TO_TIMESTAMP_TZ(LastModifiedDate || ' +0000') AS last_modified_datetime,
  Id AS ods_student_key
FROM student_seed