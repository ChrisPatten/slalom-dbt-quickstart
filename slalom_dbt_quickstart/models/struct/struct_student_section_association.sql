WITH seed_student_section_association AS (
  SELECT * FROM {{ source('edfi_raw', 'student_section_association') }}
)
SELECT
  TO_DATE(BeginDate) AS begin_date,
  LocalCourseCode AS local_course_code,
  SchoolId::integer AS school_id,
  SchoolYear::integer AS school_year,
  SectionIdentifier AS section_identifier,
  SessionName AS session_name,
  StudentUSI::integer AS student_usi,
  TO_DATE(EndDate) AS end_date,
  HomeroomIndicator::boolean AS homeroom_indicator,
  TO_TIMESTAMP_TZ(CreateDate || ' +0000') AS create_datetime,
  TO_TIMESTAMP_TZ(LastModifiedDate || ' +0000') AS last_modified_datetime,
  Id AS ods_student_section_association_id
FROM seed_student_section_association
