WITH date_spine AS (
  SELECT 
    dateadd(day, seq4(), '2010-08-23'::date) AS date_val
  FROM 
    TABLE(generator(rowcount=>278)) -- End date 2011-05-27
  WHERE dayname(date_val) NOT IN ('Sat', 'Sun')
)
, date_exceptions AS (
  SELECT
    date_val,
    calendar_event
  FROM (
    VALUES
      ('2010-09-06'::date, 'No school'),
      ('2010-09-07'::date, 'No school'),
      ('2010-10-11'::date, 'No school'),
      ('2010-11-11'::date, 'No school'),
      ('2010-11-24'::date, 'Early release'),
      ('2010-11-25'::date, 'No school'),
      ('2010-11-26'::date, 'No school'),
      ('2010-12-24'::date, 'No school'),
      ('2010-12-27'::date, 'No school'),
      ('2010-12-28'::date, 'No school'),
      ('2010-12-29'::date, 'No school'),
      ('2010-12-30'::date, 'No school'),
      ('2010-12-31'::date, 'No school'),
      ('2011-01-03'::date, 'No school'),
      ('2011-01-17'::date, 'No school'),
      ('2011-02-21'::date, 'No school'),
      ('2011-02-22'::date, 'No school'),
      ('2011-02-23'::date, 'No school'),
      ('2011-02-24'::date, 'No school'),
      ('2011-02-25'::date, 'No school'),
      ('2011-03-17'::date, 'No school'),
      ('2011-04-18'::date, 'No school'),
      ('2011-04-19'::date, 'No school'),
      ('2011-04-20'::date, 'No school'),
      ('2011-04-21'::date, 'No school'),
      ('2011-04-22'::date, 'No school')
  ) AS v (date_val, calendar_event)
)
, result AS (
  SELECT
      s.date_val,
      coalesce(e.calendar_event, 'In session') AS comb_calendar_event,
      CASE comb_calendar_event
        WHEN 'In session' THEN 1.0
        WHEN 'Early release' THEN 0.5
        ELSE 0
      END AS calendar_val
  FROM date_spine AS s
  LEFT JOIN date_exceptions AS e
      ON e.date_val = s.date_val
)
SELECT
  date_val AS calendar_date,
  2011 AS school_year,
  comb_calendar_event AS calendar_event,
  calendar_val AS instructional_day_value
FROM result