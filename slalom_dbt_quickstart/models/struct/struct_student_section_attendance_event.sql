WITH seed_student_section_attendance_event AS (
  SELECT * FROM {{ source('edfi_raw', 'student_section_attendance_event') }}
)
SELECT
  AttendanceEventCategoryDescriptorId::integer AS attendance_event_category_descriptor_id,
  TO_DATE(EventDate) AS event_date,
  LocalCourseCode AS local_course_code,
  SchoolId::integer AS school_id,
  SchoolYear::integer AS school_year,
  SectionIdentifier AS section_identifier,
  SessionName AS session_name,
  StudentUSI::integer AS student_usi,
  AttendanceEventReason AS attendance_event_reason,
  EducationalEnvironmentDescriptorId::integer AS educational_environment_descriptor_id,
  TO_TIMESTAMP_TZ(CreateDate || ' +0000') AS create_datetime,
  TO_TIMESTAMP_TZ(LastModifiedDate || ' +0000') AS last_modified_datetime,
  Id AS ods_student_section_attendance_event_id
FROM seed_student_section_attendance_event
