WITH struct_school AS (
  SELECT * FROM {{ ref('struct_school') }}
)
, struct_education_organization AS (
  SELECT * FROM {{ ref('struct_education_organization') }}
)
SELECT
  {{ dbt_utils.surrogate_key(['struct_school.school_id']) }} AS dim_school_key,
  struct_school.school_id AS school_id,
  struct_school.district_id AS district_id,
  struct_education_organization.ods_education_organization_id AS ods_education_organization_id,
  struct_education_organization.name_of_institution AS school_name,
  struct_education_organization.short_name_of_institution AS school_short_name,
  struct_education_organization.web_site AS web_site
FROM struct_school
JOIN struct_education_organization
  ON struct_education_organization.education_organization_id = struct_school.school_id
    AND struct_education_organization.discriminator = 'edfi.School'