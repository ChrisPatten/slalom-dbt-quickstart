WITH struct_student AS (
  SELECT * FROM {{ ref('struct_student') }}
)
SELECT
  {{ dbt_utils.surrogate_key(['student_id']) }} AS dim_student_key,
  student_usi,
  last_surname || IFF(generation_code_suffix IS NULL, '', ' ' || generation_code_suffix) || ', ' ||
    IFF(personal_title_prefix IS NULL, '', personal_title_prefix || ' ') ||
    first_name || IFF(middle_name IS NULL, '', ' ' || middle_name) AS student_name,
  birth_date,
  student_id    
FROM struct_student