WITH student_section AS (
  SELECT * FROM {{ ref('struct_student_section_association') }}
)
, attendance_event AS (
  SELECT * FROM {{ ref('struct_student_section_attendance_event') }}
)
, school_calendar AS (
  SELECT * FROM {{ ref('struct_school_calendar') }}
)
, dim_student AS (
  SELECT * FROM {{ ref('dim_student') }}
)
, dim_school AS (
  SELECT * FROM {{ ref('dim_school') }}
)
SELECT
  school_calendar.calendar_date,
  dim_student.dim_student_key,
  dim_school.dim_school_key,
  school_calendar.instructional_day_value,
  ROUND(COUNT(DISTINCT student_section.section_identifier) * school_calendar.instructional_day_value) AS eligible_periods,
  SUM(IFF(attendance_event.attendance_event_reason = 'Absent excused', 1, 0)) AS excused_absense_periods,
  SUM(IFF(attendance_event.attendance_event_reason = 'Absent unexcused', 1, 0)) AS unexcused_absense_periods,
  SUM(IFF(attendance_event.attendance_event_reason = 'Tardy', 1, 0)) AS tardy_periods
FROM school_calendar
CROSS JOIN dim_student
INNER JOIN student_section
  ON (school_calendar.calendar_date BETWEEN student_section.begin_date AND student_section.end_date)
    AND dim_student.student_usi = student_section.student_usi
LEFT JOIN attendance_event
  ON dim_student.student_usi = attendance_event.student_usi
    AND student_section.section_identifier = attendance_event.section_identifier
    AND school_calendar.calendar_date = attendance_event.event_date
INNER JOIN dim_school
  ON dim_school.school_id = student_section.school_id
WHERE school_calendar.instructional_day_value > 0
GROUP BY 1, 2, 3, 4